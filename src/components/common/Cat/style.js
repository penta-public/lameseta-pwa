import styled from 'styled-components';
import { ReactComponent as ScrollDown } from '../SVG/scrolldown.svg';

export const CatWrapper = styled.div`
	position: relative;
	width: 100%;
`;
export const ScrollDownWrapper = styled(ScrollDown)`
  position: absolute;
  width: 7%;
  right: 0px;
  top: -4.5rem;
  opacity: 0.8;
`;
export const IconScroll = styled.div`
	right: 0px;

	width: 25px;
	height: 50px;
	top: -4.5rem;
	border-radius: 25px;
	border: 2px solid #f8fcf7;
	&:before {
		position: absolute;
		left: 50%;
		content: '';
		width: 8px;
		height: 8px;
		background: #fff;
		margin-left: -4px;
		top: 8px;
		border-radius: 4px;
		animation-duration: 3s;
		animation-iteration-count: infinite;
		animation-name: scroll;
	}

	@-moz-keyframes scroll {
		0% {
			opacity: 1;
		}
		100% {
			opacity: 0;
			transform: translateY(30px);
		}
	}
	@-webkit-keyframes scroll {
		0% {
			opacity: 1;
		}
		100% {
			opacity: 0;
			transform: translateY(30px);
		}
	}
	@-o-keyframes scroll {
		0% {
			opacity: 1;
		}
		100% {
			opacity: 0;
			transform: translateY(30px);
		}
	}
	@keyframes scroll {
		0% {
			opacity: 1;
		}
		100% {
			opacity: 0;
			transform: translateY(30px);
		}
	}
`;
