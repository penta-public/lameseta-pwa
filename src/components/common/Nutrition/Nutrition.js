import React from 'react';
import { ReactComponent as NutritionData } from '../SVG/nutrition_320x85.svg';

import { NutritionWrapper, Title } from './style';
const Nutrition = (props) => {
	return (
		<NutritionWrapper>
			<NutritionData />
			<Title>(per serve, approx.)</Title>
		</NutritionWrapper>
	);
};

export default Nutrition;
