import React, { Fragment, useEffect, useState, useContext } from 'react';
import styled from 'styled-components';
import { Section, Container } from '@components/global';
import Ink from 'react-ink';
import Button from '@components/common/Button';
import { Form, FormField } from 'react-hooks-form';
import { getFirebase } from '../../services/Firebase';
//import NotyfContext from '../context/NotyfContext';
import SendOutlinedIcon from '@material-ui/icons/SendOutlined';
const Contact = () => {
  const [Firebase, setFirebase] = useState(null);
  //const notyf = useContext(NotyfContext);

  useEffect(() => {
    const firebaseApp = import('firebase/app');
    //const firebaseDatabase = import('firebase/database');
    const firebaseFirestore = import('firebase/firestore');
    Promise.all([firebaseApp, firebaseFirestore]).then(([firebase]) => {
      const _firebase = getFirebase(firebase).firestore();
      setFirebase(_firebase);
      // do something with `database` here,
      // or store it as an instance variable or in state
      // to do stuff with it later
    });
  }, []);
  const handleSubmit = async d => {
    try {
      const url = `https://us-central1-${process.env.GATSBY_FIREBASE_PROJECT_ID}.cloudfunctions.net/contact`;
      d = Object.assign({ fullName: '', email: '', message: '' }, d);
      const body = JSON.stringify(d);
      fetch(url, {
        method: 'post',
        body: body,
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
        .then(function(response) {
          return response.json();
        })
        .then(function(data) {
          //data is returned from cloudfunction, d is the form data
          console.log('Created Gist:', data);
          if (Firebase) {
            const userRef = Firebase.collection('messages').add(d);
          }
          //  notyf.success({ duration: 3000, message: 'Send us <b>an email</b>' });
        });
    } catch (err) {
      console.log(err);
    }
  };
  const ContactWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    padding: 2rem;
    align-items: center;
  `;

  const ContactText = styled.h2`
    padding: 0 0 1rem 0;
  `;
  const FormWrapper = styled(Form)``;
  const FormFieldWrapper = styled(FormField)`
    outline: 0;
    border: transparent;
    background: rgba(255, 255, 255, 1);
    width: 100%;
    padding: 0.5rem 0.5rem 0.5rem 1.5rem;
    margin-bottom: 1rem;
    ${props => props.theme.font_size.large};
    border-radius: 2rem;
    ::placeholder {
      color: ${props => props.theme.color.black.regular};
      opacity: 1;
    }
  `;
  const ink = {
    duration: 2000,
    radius: 350,
    style: { color: '#ffc34d' },
    opacity: 0.8,
    recenter: false,
  };
  const ButtonOuterWrapper = styled.div``;
  const ButtonWrapper = styled(Button)`
    border-radius: 50%;
    background-color: rgba(255, 255, 255, 1);
    color: ${props => props.theme.color.green.regular};
    left: 1rem;
    &:hover {
      color: ${props => props.theme.color.green.light};
      background-color: ${props => props.theme.color.orange.regular};
    }
  `;

  const SendOutlinedIconWrapper = styled(SendOutlinedIcon)`
    &:hover {
      htmlcolor: ${props => props.theme.color.green.light};
    }
  `;
  return (
    <Section id="contact">
      <Container>
        <ContactWrapper>
          <ContactText>To Learn More About LaMeseta</ContactText>
          <FormWrapper onSubmit={handleSubmit}>
            <FormFieldWrapper
              component="input"
              name="fullName"
              placeholder="Name"
              type="text"
            />
            <FormFieldWrapper
              component="input"
              name="email"
              placeholder="eMail"
              type="email"
            />
            <FormFieldWrapper
              component="textarea"
              name="message"
              placeholder="Message"
              rows={6}
            />
            <ButtonOuterWrapper>
              <ButtonWrapper type="submit">
                <SendOutlinedIconWrapper fontSize="large" />
                <Ink {...ink} />
              </ButtonWrapper>
            </ButtonOuterWrapper>
          </FormWrapper>
        </ContactWrapper>
      </Container>
    </Section>
  );
};

export default Contact;
