import styled from 'styled-components';

import { Container } from '@components/global';
import ico from '../SVG/ico_24.svg';

export const Nav = styled.nav`
  padding: 4px 1rem;
  background-color: rgba(255, 255, 255, 0.94);
  position: fixed;
  width: 100%;
  top: 0;
  z-index: 1000;
`;

export const BurgerButton = styled.button`
  color: ${props => props.theme.color.black.regular};
`;

export const StyledContainer = styled(Container)`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const NavListWrapper = styled.div`
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
    display: flex;
    flex-direction: row;

    ${({ mobile }) =>
      mobile &&
      `
        flex-direction: column;
        margin-top: 1em;

        > ${NavItem} {
          margin: 0;
          margin-top: 0.75em;
        }
      `};
  }
`;

export const NavItem = styled.li`
  margin: 0 0.75em;
  padding: 1rem;
  font-family: ${props => props.theme.font.secondary};
  color: ${props => props.theme.color.black.regular};
  ${props => props.theme.font_size.large};

  a {
    text-decoration: none;
    color: ${props => props.theme.color.black.regular};
  }
  &:hover,
  &:active {
    a {
      opacity: 1;
      color: ${props => props.theme.color.black.regular};
    }
  }
`;

export const MobileMenu = styled.div`
  width: 100%;
  background: ${props => props.theme.color.white.regular};
`;

export const Brand = styled.div`
  font-family: ${props => props.theme.font.primary};
  ${props => props.theme.font_size.large};
  color: ${props => props.theme.color.black.regular};
  background-image: url(${ico});
  background-repeat: no-repeat;
  background-position: left center;
  padding-left: 30px;
  padding-top: 2px;
  min-height: 28px;
  @media (min-width: ${props => props.theme.screen.md}) {
    ${props => props.theme.font_size.xlarge};
  }
`;

export const Mobile = styled.div`
  display: none;

  @media (max-width: ${props => props.theme.screen.md}) {
    display: block;
  }

  ${props =>
    props.hide &&
    `
    display: block;

    @media (max-width: ${props.theme.screen.md}) {
      display: none;
    }
  `};
`;
