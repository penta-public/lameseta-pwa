const theme = {
  //penta purple: 4c3380
  //light green: #77cc6d
  font: {
    //  primary: `'Prata', serif`,
    primary: `sans-serif`,
    //  secondary: `'Average', serif`,
    secondary: `sans-serif`,
  },
  font_size: {
    xsmall: 'font-size: 0.79rem; line-height:1.5',
    small: 'font-size: 0.9rem; line-height: 1.5',
    regular: 'font-size: 1.05rem; line-height: 1.5',
    large: 'font-size: 1.15rem; line-height: 1.5',
    larger: 'font-size: 1.3rem; line-height: 1.5',
    xlarge: 'font-size: 1.6rem; line-height: 1.5',
  },
  color: {
    white: {
      regular: '#FFFFFF',
      dark: '#F6F6F6',
    },
    black: {
      lighter: '#ABA8AF',
      light: '#564F62',
      regular: '#211E26',
    },
    primary: '#77cc6d',
    green: {
      light: '#f8fcf7',
      lighter: '#b8e4b4',
      regular: '#77cc6d',
      shade: '#55853D',
      darker: '#4e7a38',
      dark: '#507D39',
      xdark: '#3C5D2B',
    },
    orange: {
      light: '#ffe6b3',
      lighter: '#ffcc66',
      regular: '#ffc34d',
      darker: '#B37700',
      dark: '#805500',
    },
    red: {
      darker: '#A52A2A',
    },
    purple: {
      light: '#906ec4',
      regular: '#6943a3',
      dark: '#462c6d',
    },
  },
  screen: {
    xs: '319px',
    sm: '541px',
    md: '768px',
    lg: '1224px',
    xl: '1760px',
  },
};

export default theme;
