import styled from 'styled-components';

export const Container = styled.div`
  max-width: 2400px;
  width: 100%;
  margin: 0 0;
  padding: 0 0;
`;

export const Section = styled.section`
  overflow: hidden;

  @media (max-width: ${props => props.theme.screen.md}) {
    padding: 0;
  }

  ${props =>
    props.accent &&
    `background-color: ${
      props.accent === 'secondary'
        ? props.theme.color.white.dark
        : props.theme.color.primary
    }`};
`;
