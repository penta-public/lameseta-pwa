import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
	position: relative;
	cursor: pointer;
	padding: 0.75rem;
	&.focus,
	&:focus {
		outline: 0;
	}
	&:hover {
		outline: 0;
	}
`;

const Button = ({ children, label, ...rest }) => {
	return <StyledButton {...rest}>{children}</StyledButton>;
};

export default Button;
