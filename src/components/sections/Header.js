import React from 'react';
import styled from 'styled-components';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import BackgroundImage from 'gatsby-background-image';
import { Container } from '@components/global';
import { Producer } from '../common/Producer';
import { Rating } from '../common/Rating';

import { ReactComponent as Logo } from '../common/SVG/laMeseta.svg';
//import ExternalLink from '@common/ExternalLink';
//import { Cat } from '../common/Cat';
//import { Nutrition } from '../common/Nutrition';
//import { Badges } from '../common/Badges';
//import { Links } from '../common/Links';
import {
  BadgesWrapper,
  CertificatesWrapper,
  CertifiedWrapper,
} from '../common/Badges';

const Header = () => {
  const { producer_lameseta } = useStaticQuery(graphql`
    query {
      producer_lameseta: file(
        sourceInstanceName: { eq: "story" }
        name: { eq: "rodrigo-flores-T5qjs-63kqQ-unsplash2" }
      ) {
        childImageSharp {
          fluid(maxWidth: 2000) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  `);
  return (
    <Container>
      <HeaderWrapper id="header">
        <ContainerWrapper>
          <LogoOuterWrapper>
            <LogoWrapper />
          </LogoOuterWrapper>
          <BackgroundWrapper
            fluid={producer_lameseta.childImageSharp.fluid}
            backgroundColor={'#77cc6d'}
          >
            <InnerWrapper></InnerWrapper>
            <Marquee>
              We Pick the Best
              <br />
              Cherries of Coffee by Hand
            </Marquee>
          </BackgroundWrapper>
          <Grid>
            <Card
              i="A"
              title="Provenance"
              img={null}
              videoSrcURL={null}
              videoTitle={null}
            >
              <Producer />
            </Card>
            <Card
              i="B"
              title="Trust Score (5/5)"
              img={null}
              videoSrcURL={null}
              videoTitle={null}
            >
              <Rating />
            </Card>
            <Card
              i="C"
              title="Certifications"
              img={null}
              videoSrcURL={null}
              videoTitle={null}
            >
              <BadgesWrapper>
                <CertificatesWrapper />
              </BadgesWrapper>
            </Card>
            <Card
              i="D"
              title="Blockchain Secured"
              img={null}
              videoSrcURL={null}
              videoTitle={null}
            >
              <BadgesWrapper>
                <CertifiedWrapper />
              </BadgesWrapper>
            </Card>
          </Grid>
          <Rule />
        </ContainerWrapper>
      </HeaderWrapper>
    </Container>
  );
};

const Card = props => {
  const CardWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    @media (min-width: ${props => props.theme.screen.xs}) {
      flex: 0 1 100%;
    }
    @media (min-width: ${props => props.theme.screen.md}) {
      flex: 0 1 calc(50% - 1rem);
      max-width: 368px;
    }
    @media (min-width: ${props => props.theme.screen.lg}) {
      flex: 0 1 calc(33% - 1rem);
      max-width: 368px;
    }
    @media (min-width: ${props => props.theme.screen.xl}) {
      flex: 0 1 calc(25% - 1rem);
      max-width: 368px;
    }
  `;
  const CardInner = styled.div`
    width: 100%;
    max-width: 500px;
    @media (min-width: ${props => props.theme.screen.xs}) {
      padding: 0 1rem 0rem 1rem;
    }
    @media (min-width: ${props => props.theme.screen.md}) {
      padding: 0 0 0rem 0;
    }
  `;
  const H2Wrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    padding: 1rem 0.5rem 0 0;
    width: 100%;
    @media (max-width: ${props => props.theme.screen.md}) {
      align-items: baseline;
    }
  `;
  const H2 = styled.h2`
    padding: 0 0 0 1rem;
  `;
  const H2I = styled.span`
    background-color: ${props => props.theme.color.green.regular};
    color: white;
    ${props => props.theme.font_size.xlarge};
    border-radius: 4px;
    padding: 0 0.5rem;
  `;
  const P = styled.div`
    padding: 1rem 0 0 0;
    text-align: justify;
    height: auto;
    display: flex;
    flex-direction: column
    justify-content: flex-start;
    align-items: center;
  `;
  const ArtImg = styled(Img)`
    margin: 0;
    max-width: 500px;
    width: 100%;
    max-height: 323px;
  `;
  const Video = styled.div``;
  return (
    <CardWrapper>
      {props.img && <ArtImg fluid={props.img} />}
      {props.videoSrcURL && (
        <Video>
          <iframe
            width="100%"
            height="315px"
            src={props.videoSrcURL}
            title={props.videoTitle}
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            frameBorder="0"
            webkitallowfullscreen="true"
            mozallowfullscreen="true"
            allowFullScreen
          />
        </Video>
      )}
      <CardInner>
        <H2Wrapper>
          <H2I>{props.i}</H2I>
          <H2>{props.title}</H2>
        </H2Wrapper>
        {props.p && <P>{props.p}</P>}
        {props.children && <P>{props.children}</P>}
      </CardInner>
    </CardWrapper>
  );
}; //eo card

const Grid = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  align-items: flex-start;
  padding-top: 20px;
`;
const Rule = styled.hr`
  margin-top: 25px;
  border: 1px solid rgba(230, 230, 230, 0.5);
  border-radius: 2px;
  width: 80%;
`;
/* const Provenance = styled.div`
  width: 100%;
  padding: 20px 15px 0 15px;
  background-color: white;
`; */
const Marquee = styled.div`
  color: ${props => props.theme.color.white.regular};
  text-align: right;
  line-height: 1.2;
  margin-top: -30px;
  font-weight: 500;
  overflow: visible;
  white-space: nowrap;
  @media (min-width: ${props => props.theme.screen.xs}) {
    font-size: 1.5rem;
    padding-right: 20px;
  }
  @media (min-width: ${props => props.theme.screen.md}) {
    font-size: 1.8rem;
    padding-right: 30px;
  }
  @media (min-width: ${props => props.theme.screen.lg}) {
    font-size: 2.2rem;
    padding-right: 50px;
  }
  @media (min-width: ${props => props.theme.screen.xl}) {
    font-size: 2.5rem;
    padding-right: 60px;
  }
`;

const InnerWrapper = styled.div`
  padding: 1rem;
  min-height: 350px;
  width: 100%;
  @media (min-width: ${props => props.theme.screen.xs}) {
    height: 350px;
  }
  @media (min-width: ${props => props.theme.screen.md}) {
    height: 440px;
  }
  @media (min-width: ${props => props.theme.screen.lg}) {
    height: 510px;
  }
  @media (min-width: ${props => props.theme.screen.xl}) {
    height: 600px;
  }
`;
const HeaderWrapper = styled.header`
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
  padding-top: 60px;
  color: ${props => props.theme.color.black.regular};
  background-color: ${props => props.theme.color.white.regular};
  @media (min-width: ${props => props.theme.screen.md}) {
    padding-top: 60px;
    h3 {
      ${props => props.theme.font_size.larger};
    }
  }
  @media (min-width: ${props => props.theme.screen.lg}) {
    flex-flow: row wrap;
    justify-content: space-around;
    padding-top: 100px;
  }
`;
const ContainerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  user-select: none;
  width: 100%;
`;
const LogoOuterWrapper = styled.div`
  width: 100%;
  padding-bottom: 10px;
`;
const LogoWrapper = styled(Logo)`
  width: 100%;
  padding: 0;
  height: 80px;
`;
const BackgroundWrapper = styled(BackgroundImage)`
  background-position: center center;
  &:before,
  &:after {
    background-position: center center;
  }
  @media (min-width: ${props => props.theme.screen.xs}) {
    height: 400px;
  }
  @media (min-width: ${props => props.theme.screen.md}) {
    height: 500px;
  }
  @media (min-width: ${props => props.theme.screen.lg}) {
    height: 600px;
  }
  @media (min-width: ${props => props.theme.screen.xl}) {
    height: 700px;
  }
`;

/* const Art = styled.figure`
  width: 100%;
  margin: 0;

  > div {
    width: 120%;
    //     margin-bottom: -4.5%;
 
    @media (max-width: ${props => props.theme.screen.md}) {
      width: 100%;
    }
  }
`;

const Title = styled.h1`
  text-align: center;
  color: ${props => props.theme.color.black.regular};
`;

const Subtitle = styled.h3`
  text-align: center;
  color: ${props => props.theme.color.black.regular};
`;

const StyledExternalLink = styled(ExternalLink)`
  color: inherit;
  text-decoration: none;

  &:hover {
    color: ${props => props.theme.color.black.regular};
  }
`;
 */
export default Header;
