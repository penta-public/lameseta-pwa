import MontserratMedium from './montserrat/montserrat-medium-webfont.woff';
import LoraRegular from './lora/LoraRegular';

export {MontserratMedium, LoraRegular}