import React from 'react';
import 'notyf/notyf.min.css';
import { Notyf } from 'notyf';
const NotyfContext = React
  .createContext
  /* new Notyf({
		duration: 1000,
		types: [
			{
				type: 'success',
				className: 'notyfX',
				backgroundColor: 'orange',
				duration: 60000
			}, //eo error
			{
				className: 'notyfX',
				type: 'error',
				duration: 30000
			} //eo error
		] //eo types
	}) //eo new Notyf */
  ();

export const NotyfProvider = NotyfContext.Provider;
export const NotyfConsumer = NotyfContext.Consumer;
export default NotyfContext;
