import React from 'react';
import styled from 'styled-components';
import { ReactComponent as Logo } from '../SVG/JackBrownProduce.svg';
import { useStaticQuery, graphql } from 'gatsby';

export const DataWrapper = () => {
	const data = useStaticQuery(graphql`
		query {
			hero_apples: file(sourceInstanceName: { eq: "story" }, name: { eq: "sydney-rae-_5TGspSCIdw-unsplash" }) {
				childImageSharp {
					fluid(maxWidth: 500) {
						...GatsbyImageSharpFluid_withWebp_tracedSVG
					}
				}
			}
		}
	`);

	const Wrapper = styled.div`
		width: 100%;
		height: 150px;
		background-image: url(${data.hero_apples.childImageSharp.fluid.base64});
		background-size: cover;
	`;

	return <Wrapper />;
};

export const ProducerWrapper = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	width: 100%;
	height: 65px;
`;

export const LogoWrapper = styled(Logo)`
  width: 35%;
`;

export const ProducerData = styled.div`
	max-width: 65%;
	text-align: right;
	color: ${(props) => props.theme.color.green.xdark};
	${(props) => props.theme.font_size.xsmall};
`;
