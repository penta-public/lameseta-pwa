import React from 'react';
import styled from 'styled-components';
import { Rating } from '../Rating';
const Producer = () => {
  return (
    <ProducerData>
      Product: Premium Colombian Coffee
      <br />
      From: Chinchina, Colombia
      <br />
      Producer: La Meseta
      <br />
      Shelving Date: March 25, 2019
      <br />
      Best Before: April 4, 2020
      <br />
      <Trust>
        Trust Score: <Rating />
      </Trust>
    </ProducerData>
  );
};

const Trust = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  padding-top: 10px;
`;
export const ProducerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-content: center;
`;

export const ProducerData = styled.div`
  color: ${props => props.theme.color.black.regular};
  ${props => props.theme.font_size.regular};
`;

export default Producer;
