import React, {Fragment} from 'react';
import { config, useSpring, animated } from 'react-spring';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import {
	TapLabel,
	ProgressWrapper,
	PercentWrapper,
	CircleWrapper,
	CircleInner,
	CircleOuter,
	PercentLabel,
	ProgressLabel
} from './style';

const Circle = (props) => {
	const size = props.size;
	const progress = props.progress;

	const center = size / 2;
	const strokeWidth = size * 0.1;
	const radius = size / 2 - strokeWidth / 2;
	const circumference = 2 * Math.PI * radius;
	const offset = (100 - progress) / 100 * circumference;

	const style = {
		strokeDashoffset: offset
	};

	return (
		<CircleWrapper width={size} height={size} viewBox={`0 0 ${size} ${size}`}>
			<CircleOuter cx={center} cy={center} r={radius} strokeWidth={strokeWidth} />
			<CircleInner
				style={style}
				cx={center}
				cy={center}
				r={radius}
				strokeWidth={strokeWidth}
				strokeDasharray={circumference}
			/>
		</CircleWrapper>
	);
};

const Percent = (props) => {
	let progress = props.progress.toString().match(/^-?\d+(?:\.\d{0,1})?/)[0];
	return (
		<PercentWrapper>
			<AnchorLink href={`#about`} style={{ textDecoration: 'none' }}>
				<PercentLabel>TRUST</PercentLabel>
				<div className="percent__content">{progress}%</div>
				<PercentLabel>
					SCORE<TapLabel>Tap</TapLabel>
				</PercentLabel>
			</AnchorLink>
		</PercentWrapper>
	);
};

const Progress = () => {
	const props = useSpring({ config: config.molasses, size: 240, progress: 100, from: { progress: 0 } });
	const AnimatedCircle = animated(Circle);
	const AnimatedPercent = animated(Percent);
	return (
		<ProgressWrapper>
			<AnimatedCircle {...props} />
			<AnimatedPercent {...props} />
		</ProgressWrapper>
	);
};

const CircularGauge = (props) => {
	return (
		<Fragment>
			<Progress />
			<ProgressLabel>
			</ProgressLabel>
		</Fragment>
	);
};

export default CircularGauge;
