import React, { useState, useRef, useEffect } from 'react';
import styled from 'styled-components';
import { InView } from 'react-intersection-observer';
import { ReactComponent as Certified } from '../SVG/blockchainCertified.svg';
import { ReactComponent as Certificates } from '../SVG/certificates.svg';

export const BadgesWrapper = styled.div`
  text-align: center;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
`;
let visibility = {};
export const CertificatesWrapper = styled(Certificates)``;
export const CertifiedWrapper = props => {
  const [isVisible, setIsVisible] = useState(false);

  const onChange = (inView, entry) => {
    if (!isVisible && inView) setIsVisible(true);
  };
  const CertifiedStyledWrapper = styled(Certified)`
    #checkmark {
      fill: none;
      stroke: #77c44c;
      stroke-linecap: round;
      stroke-miterlimit: 10;
      stroke-width: 6.9;
      stroke-dasharray: 400;

      animation: checkmark-stroke 2s forwards;
      animation-timing-function: ease-in;
      animation-delay: 0.5s;
      stroke-linejoin: round;
      opacity: 0;
      animation-play-state: ${isVisible ? 'running' : 'paused'};
    }

    @keyframes checkmark-stroke {
      0% {
        stroke-dashoffset: 400;
        opacity: 1;
      }
      100% {
        stroke-dashoffset: 0;
        opacity: 1;
      }
    }
  `;
  return (
    <InView as="div" onChange={onChange}>
      <CertifiedStyledWrapper />
    </InView>
  );
}; //eo CertifiedWrapper

/* const Badges = props => {
  return (
    <BadgesWrapper>
      <CertificatesWrapper />
      <CertifiedWrapper />
    </BadgesWrapper>
  );
};

export default Badges; */
