import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import { ScrollDownWrapper, CatWrapper } from './style';

const Cat = () => {
	return (
		<CatWrapper>
			<AnchorLink href={`#about`} style={{ textDecoration: 'none' }}>
				<ScrollDownWrapper />
			</AnchorLink>
		</CatWrapper>
	);
};

export default Cat;
