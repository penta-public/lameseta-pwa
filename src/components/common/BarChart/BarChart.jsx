import React, { useState } from 'react';
import styled, { keyframes } from 'styled-components';
import { InView } from 'react-intersection-observer';

const BarChart = () => {
  const [isVisible, setIsVisible] = useState(false);

  const onChange = (inView, entry) => {
    if (!isVisible && inView) setIsVisible(true);
  };
  const chartItems = [
    {
      key: 'product-transparency',
      className: 'chart--1',
      style: {
        width: '100%',
        background: `linear-gradient(to right, #FF963B 50%, #FFBA3F 100%)`,
      },
      label: 'Product Transparency',
    },
    {
      key: 'sustainable-farming',
      className: 'chart--2',
      style: {
        width: '100%',
        background: `linear-gradient(to right, #FFBA3F 50%, #FFD83F 100%)`,
      },
      label: 'Sustainable Farming',
    },
    {
      key: 'fair-labor',
      className: 'chart--3',
      style: {
        width: '100%',
        background: `linear-gradient(to right, #FFD83F 50%, #FFF43F 100%)`,
      },
      label: 'Fair Labor',
    },
    {
      key: 'climate-responsiveness',
      className: 'chart--4',
      style: {
        width: '100%',
        background: `linear-gradient(to right, #a1e600 50%, #C6FF44 100%)`,
      },
      label: 'Climate Responsiveness',
    },
    {
      key: 'blockchain-validated',
      className: 'chart--5',
      style: {
        width: '100%',
        background: `linear-gradient(to right, #77cc6d 50%, #C6FF44 100%)`,
      },
      label: 'Blockchain Validated',
    },
  ];
  //#805500 507d39
  const ChartHorizontal = styled.ul`
    overflow: hidden;
    padding-left: 0;
  `;
  const ChartBar = styled.li`
    $border-rad: 4px;
    height: 30px;
    margin-bottom: 10px;
    list-style-type: none;
    //background: linear-gradient(to right, #77cc6d 3%, #77cc6d 100%);
    border-top-right-radius: $border-rad;
    border-bottom-right-radius: $border-rad;
    opacity: 0;
    animation: ${morph} 2.5s ease forwards;
    animation-play-state: ${isVisible ? 'running' : 'paused'};

    &.chart--1 {
      animation-delay: 0.1s;
    }
    &.chart--2 {
      animation-delay: 0.3s;
    }
    &.chart--3 {
      animation-delay: 0.5s;
    }
    &.chart--4 {
      animation-delay: 0.7s;
    }
    &.chart--5 {
      animation-delay: 0.9s;
    }

    .chart-label {
      padding-left: 10px;
      line-height: 30px;
      color: white;
    }
  `;
  return (
    <InView as="div" onChange={onChange}>
      <Skills>
        <Charts>
          <Chart className="chart--dev">
            {/* <ChartTitle>Development</ChartTitle> */}
            <ChartHorizontal>
              {chartItems.map(item => (
                <ChartBar
                  key={`${item.key}`}
                  className={item.className}
                  style={item.style}
                >
                  <span className="chart-label">
                    {item.label}: {item.style.width}
                  </span>
                </ChartBar>
              ))}
            </ChartHorizontal>
          </Chart>
        </Charts>
      </Skills>
    </InView>
  );
};

//Keyframes
const morph = keyframes`
    0% {
        transform: translateX(-200%);
        opacity: 1;
    }
  100% {
        transform: translateX(0);
        opacity: 1;
    }
`;

//Demo
const Skills = styled.div`
  width: 100%;
  max-width: 960px;
  height: 250px;
  margin: auto;

  position: relative;
`;

const Charts = styled.div`
  width: calc(100% - 1rem);
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 10;
`;

const Chart = styled.div`
  margin: 30px 0 0;

  &: first-child {
    margin: 0;
  }
`;

export default BarChart;
