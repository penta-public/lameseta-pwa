import styled from 'styled-components';

export const Title = styled.h3`
	color: ${(props) => props.theme.color.green.light};
	${(props) => props.theme.font_size.small};

	text-align: right;
`;
export const NutritionWrapper = styled.div`
width: 100%;
padding: 0;

@media (min-width: ${(props) => props.theme.screen.sm}) {
    text-align:center
    max-width: 50%;
}
`;
