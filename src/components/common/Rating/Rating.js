import React, { Fragment } from 'react';
import styled from 'styled-components';
import { config, useSpring, animated } from 'react-spring';

const Rating = () => {
  const RatingWrapper = styled.div`
    display: inline-flex;
    flex-direction: row-reverse;
    justify-content: center;
    padding: 0 0 0 0.5rem;
    input {
      display: none;
    }
  `;

  const Label = styled.label`
    position: relative;
    max-width: 1.1em;
    color: ${props => props.theme.color.orange.regular};
    cursor: pointer;
    opacity: 0;
    padding: 0 5px;
    @media (min-width: ${props => props.theme.screen.xs}) {
      font-size: 2.5rem;
    }
    @media (min-width: ${props => props.theme.screen.md}) {
      font-size: 2.5rem;
    }
    @media (min-width: ${props => props.theme.screen.lg}) {
      font-size: 3rem;
    }
    @media (min-width: ${props => props.theme.screen.xl}) {
      font-size: 3rem;
    }
  `;
  const AnimatedLabel = animated(Label);
  const stars = [5, 4, 3, 2, 1];
  const Stars = stars.map((i, index) => {
    const props = useSpring({
      config: config.molasses,
      reset: true,
      delay: i * 500,
      opacity: 1,
      from: { opacity: 0 },
    });
    return (
      <Fragment key={'rating_' + i}>
        <input type="radio" name="rating" value={i} id={i} />
        <AnimatedLabel htmlFor={i} style={{ ...props }}>
          ★
        </AnimatedLabel>
      </Fragment>
    );
  });
  return <RatingWrapper>{Stars}</RatingWrapper>;
};

export default Rating;
