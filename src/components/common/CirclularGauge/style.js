import styled from 'styled-components';
import apple from './apple_250_2.jpg';

export const ProgressWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 5px;
`;

export const CircleWrapper = styled.svg`
  fill: none;
  stroke-linecap: round;
  background-image: url(${apple});
  border-radius: 50%;
  background-size: contain;
`;

export const CircleOuter = styled.circle``;

export const CircleInner = styled.circle`
  stroke: ${props => props.theme.color.green.darker};
  opacity: 0.98;
  fill: none;
`;

export const PercentLabel = styled.div`
  color: ${props => props.theme.color.green.light};
  padding: 0.2rem 0;
  font-weight: 500;
`;
export const TapLabel = styled.div`
  font-style: italic;
  padding-top: 0.8rem;
`;
export const ProgressLabel = styled.div`
  text-align: center;
  transform: translateY(-150px);
  color: ${props => props.theme.color.green.light};
  font-size: 1.2rem;
  line-height: 1.3rem;
  font-weight: 500;
`;
export const PercentWrapper = styled.div`
  text-align: center;
  transform: translateY(-175px);
  .percent__content {
    color: ${props => props.theme.color.green.light};
    font-size: 3rem;
  }
`;
