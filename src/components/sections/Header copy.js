import React from 'react';
import styled from 'styled-components';
import { useStaticQuery, graphql } from 'gatsby';
//import Img from 'gatsby-image';
import BackgroundImage from 'gatsby-background-image';
import { Container } from '@components/global';
import { Producer } from '../common/Producer';
import { ReactComponent as Logo } from '../common/SVG/laMeseta.svg';
import ExternalLink from '@common/ExternalLink';
import { Cat } from '../common/Cat';
import { Nutrition } from '../common/Nutrition';
import { Badges } from '../common/Badges';
import { Links } from '../common/Links';

const Header = () => {
  const { producer_lameseta } = useStaticQuery(graphql`
    query {
      producer_lameseta: file(
        sourceInstanceName: { eq: "story" }
        name: { eq: "rodrigo-flores-T5qjs-63kqQ-unsplash2" }
      ) {
        childImageSharp {
          fluid(maxWidth: 2000) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  `);
  return (
    <Container>
      <HeaderWrapper id="header">
        <ContainerWrapper>
          <LogoOuterWrapper>
            <LogoWrapper />
          </LogoOuterWrapper>
          <BackgroundWrapper
            fluid={producer_lameseta.childImageSharp.fluid}
            backgroundColor={'#77cc6d'}
          >
            <InnerWrapper></InnerWrapper>
            <Marquee>
              We Pick the Best
              <br />
              Cherries of Coffee by Hand
            </Marquee>
            {/* <Producer />
            <InnerWrapper>
              <Nutrition />
              <Badges />
            </InnerWrapper> */}
            {/*  <Links /> */}
            {/* 						<Cat />
             */}{' '}
          </BackgroundWrapper>
          <Provenance>
            <Producer />
            <Badges />
          </Provenance>
          <Rule />
        </ContainerWrapper>
      </HeaderWrapper>
    </Container>
  );
};

const Rule = styled.hr`
  margin-top: 25px;
  border: 1px solid rgba(230, 230, 230, 0.5);
  border-radius: 2px;
  width: 80%;
`;
const Provenance = styled.div`
  width: 100%;
  padding: 20px 15px 0 15px;
  background-color: white;
`;
const Marquee = styled.div`
  color: ${props => props.theme.color.white.regular};
  text-align: right;
  ${props => props.theme.font_size.larger};
  line-height: 1.2;
  padding-right: 20px;
  margin-top: -30px;
  font-weight: 500;
  overflow: visible;
  white-space: nowrap;
`;
const HeaderWrapper = styled.header`
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
  padding-top: 60px;
  color: ${props => props.theme.color.black.regular};
  background-color: ${props => props.theme.color.white.regular};
  @media (min-width: ${props => props.theme.screen.md}) {
    padding-top: 60px;
    h3 {
      ${props => props.theme.font_size.larger};
    }
  }
  @media (min-width: ${props => props.theme.screen.lg}) {
    flex-flow: row wrap;
    justify-content: space-around;
    padding-top: 100px;
  }
`;
const ContainerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  user-select: none;
  width: 100%;
`;
const LogoOuterWrapper = styled.div`
  width: 100%;
  padding-bottom: 10px;
`;
const LogoWrapper = styled(Logo)`
  width: 100%;
  padding: 0;
  height: 80px;
`;
const BackgroundWrapper = styled(BackgroundImage)`
  background-position: center center;
  &:before,
  &:after {
    background-position: center center;
  }
  @media (min-width: ${props => props.theme.screen.xs}) {
    height: 400px;
  }
  @media (min-width: ${props => props.theme.screen.md}) {
    height: 500px;
  }
  @media (min-width: ${props => props.theme.screen.lg}) {
    height: 600px;
  }
  @media (min-width: ${props => props.theme.screen.xl}) {
    height: 700px;
  }
`;
const InnerWrapper = styled.div`
  padding: 1rem;
  min-height: 350px;
  width: 100%;
`;
const Art = styled.figure`
  width: 100%;
  margin: 0;

  > div {
    width: 120%;
    /*     margin-bottom: -4.5%;
 */
    @media (max-width: ${props => props.theme.screen.md}) {
      width: 100%;
    }
  }
`;

const Title = styled.h1`
  text-align: center;
  color: ${props => props.theme.color.black.regular};
`;

const Subtitle = styled.h3`
  text-align: center;
  color: ${props => props.theme.color.black.regular};
`;

const StyledExternalLink = styled(ExternalLink)`
  color: inherit;
  text-decoration: none;

  &:hover {
    color: ${props => props.theme.color.black.regular};
  }
`;

export default Header;
