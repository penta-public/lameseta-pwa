import styled from 'styled-components';
import BackgroundImage from 'gatsby-background-image';

export const Title = styled.h3`
	color: ${(props) => props.theme.color.green.light};
	${(props) => props.theme.font_size.regular};
	text-align: center;
	padding-bottom: 0.5rem;
`;

export const LinksWrapper = styled.div`
width: 100%;
padding-top: 0rem;

@media (min-width: ${(props) => props.theme.screen.md}) {
    text-align:center
    width: 80%;
}
`;

export const AnchorList = styled.div`
	display: flex;
	align-items: center;
	width: 100%;
`;

export const AnchorLinkWrapper = styled.a`
	cursor: pointer;
	textdecoration: 'none';
	width: 25%;
	height: 80px;
`;

export const BackgroundWrapper1 = styled(BackgroundImage)`
width: 100%;
height:  100%;
    background-position: left center;
    padding: 0;
`;

export const BackgroundWrapper2 = styled(BackgroundImage)`
width: 100%;
height:  100%;
    background-position: left center;
    padding: 0;
`;

export const BackgroundWrapper3 = styled(BackgroundImage)`
width: 100%;
height:  100%;
    background-position: left center;
    padding: 0;
`;

export const BackgroundWrapper4 = styled(BackgroundImage)`
width: 100%;
height:  100%;
    background-position: left center;
    padding: 0;
`;
