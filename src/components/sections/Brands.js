import React from 'react';
import styled from 'styled-components';
import { StaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';

import { Section, Container } from '@components/global';
import ExternalLink from '@common/ExternalLink';

const INGREDIENTS = [
	{
		key: '5 medium apples (macoun or cortland if you can get them) peeled, cored and sliced 1/2" thin'
	},
	{
		key: 'cinnamon and sugar to lightly coat the apples (~ 1-2 tsp each)'
	},
	{
		key: 'a generous Tbsp honey'
	},
	{
		key: 'a squeeze of lemon juice'
	},
	{
		key: 'Crust:'
	},
	{
		key: '1 cup flour'
	},
	{
		key: '1 cup sugar'
	},
	{
		key: '1/2 cup wheat germ'
	},
	{
		key: '1/2 cup ground walnuts'
	},
	{
		key: '3/4 cup melted butter'
	},
	{
		key: '1 egg lightly beaten'
	}
];

const UsedBy = () => (
	<StaticQuery
		query={graphql`
			query {
				apple_pie: file(sourceInstanceName: { eq: "art" }, name: { eq: "apple_pie" }) {
					childImageSharp {
						fluid(maxWidth: 1200) {
							...GatsbyImageSharpFluid_withWebp_tracedSVG
						}
					}
				}
			}
		`}
		render={(data) => (
			<Section id="recipe" accent>
				<StyledContainer>
					<StyledContainerWrapper>
						<h1>Nana's Apple Pie Recipe</h1>
						<h2>INGREDIENTS</h2>
						<IngredientsWrapper>{INGREDIENTS.map(({ key }) => <li key={key}>{key}</li>)}</IngredientsWrapper>
						<h2>PREPARATION</h2>
						<div style={{ textAlign: 'justify', lineHeight: 1.35 }}>
							Prepare the apples and coat with honey, lemon juice, cinnamon and sugar. Pour into a
							standard pie plate or equivalent-sized baker. Preheat oven to 350F, then get the crust
							ready. Mix together flour, sugar, wheat germ and walnuts. Add butter and stir until even,
							then add egg and mix until smooth. Pour it over the apples, mushing with a spatula to make
							sure it squeezes in between them. Bake for 50-60 minutes or until lightly golden.
							Spectacular if served with vanilla or pumpkin ice cream!
						</div>
						<PieWrapper>
							<Img fluid={data.apple_pie.childImageSharp.fluid} />
						</PieWrapper>
					</StyledContainerWrapper>
					<Img fluid={data.apple_pie.childImageSharp.fluid} />
				</StyledContainer>
			</Section>
		)}
	/>
);
const IngredientsWrapper = styled.ul`padding-left: 1rem;`;
const PieWrapper = styled.div`
	padding-top: 1rem;
	border-radius: 10px;
`;
const LogoGrid = styled.div`
	display: grid;
	grid-template-columns: 1fr 1fr;
	grid-gap: 64px;
	justify-items: center;
	margin-top: 96px;

	a {
		svg {
			width: 100%;
		}
	}

	@media (max-width: ${(props) => props.theme.screen.sm}) {
		grid-template-columns: 1fr;
	}
`;

const StyledContainer = styled(Container)`
  display: flex;
  justify-content: flex-start;
  position: relative;
  color: ${(props) => props.theme.color.green.light};

  @media (max-width: ${(props) => props.theme.screen.md}) {
    justify-content: center;
  }
`;

const StyledContainerWrapper = styled.div`padding: 0 0.5rem;`;

const Art = styled.figure`
	width: 600px;
	position: absolute;
	top: -12%;
	right: 50%;

	@media (max-width: ${(props) => props.theme.screen.lg}) {
		top: 0;
		right: 65%;
		width: 500px;
	}

	@media (max-width: ${(props) => props.theme.screen.md}) {
		display: none;
	}
`;

export default UsedBy;
