import React from 'react';
import styled from 'styled-components';
import { StaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';
import { BarChart } from '@components/common/BarChart';
import { Section, Container } from '@components/global';
import StarBorder from '@material-ui/icons/StarBorder';
//https://youtu.be/pmre2oP_Rn8
const About = () => (
  <StaticQuery
    query={graphql`
      query {
        story_of: file(
          sourceInstanceName: { eq: "story" }
          name: { eq: "Colombia-Coffee-Crops-Cafe-Cereza-92" }
        ) {
          childImageSharp {
            fluid(maxWidth: 760) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
        story_history: file(
          sourceInstanceName: { eq: "story" }
          name: { eq: "Colombia-Coffee-Crops-Cafe-Cereza-123" }
        ) {
          childImageSharp {
            fluid(maxWidth: 760) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
        story_supporting: file(
          sourceInstanceName: { eq: "story" }
          name: { eq: "Colombia-Coffee-Crops-Cafe-Cereza-95" }
        ) {
          childImageSharp {
            fluid(maxWidth: 760) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
        story_sensor_tracking: file(
          sourceInstanceName: { eq: "story" }
          name: {
            eq: "Colombian-Coffee-Exporters-Excelso-Green-Coffee-Specialty-Coffees-43"
          }
        ) {
          childImageSharp {
            fluid(maxWidth: 760) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
        story_device_data: file(
          sourceInstanceName: { eq: "story" }
          name: {
            eq: "Colombian-Coffee-Exporters-Excelso-Green-Coffee-Specialty-Coffees-159"
          }
        ) {
          childImageSharp {
            fluid(maxWidth: 760) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
        story_trust: file(
          sourceInstanceName: { eq: "story" }
          name: { eq: "rodrigo-flores-T5qjs-63kqQ-unsplash2" }
        ) {
          childImageSharp {
            fluid(maxWidth: 760) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
        story_reward: file(
          sourceInstanceName: { eq: "story" }
          name: { eq: "brigitte-tohm-EAay7Aj4jbc-unsplash" }
        ) {
          childImageSharp {
            fluid(maxWidth: 760) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    `}
    render={data => (
      <Section id="about">
        <Container>
          <SubContainer>
            <Grid>
              <Card
                i="1"
                title="The Story of Compañia Cafetera"
                p="Compañia Cafetera La Meseta is a family business with a lot of experience in the marketing of Colombian coffee. Reliability and performance has allowed us continued growth and market permanence since its inception in 1983. "
                img={data.story_of.childImageSharp.fluid}
                videoSrcURL={null}
                videoTitle={null}
              />
              <Card
                i="2"
                title="La Meseta's History"
                p="La Meseta began as small-scale coffee growers with a production of 10.000kg of coffee parchment in the town of Villamaria, State of Caldas, Colombia, and today the company has established itself as a strong agro-industrial complex with an average production of export type Excelso coffee (green coffee) averaging 70,000 bags per month."
                img={data.story_history.childImageSharp.fluid}
                videoSrcURL={null}
                videoTitle={null}
              />
              <Card
                i="3"
                title="Supporting our Coffee Growers"
                p="La Meseta believes in its people, in the possibility to create more employment through producing and marketing this valuable fruit that is coffee, which causes passion and pride. These motivate me every day to generate better commercial relations with the shipping companies, clients and customs agents because I am convinced that these good relationships help to make an active and assertive process."
                img={data.story_supporting.childImageSharp.fluid}
                videoSrcURL={null}
                videoTitle={null}
              />
              <Card
                i="4"
                title="A Wealth of Experience"
                p="More than 30 years of experience in both production and commercialization of Colombian coffee have allowed us to be recognized as one of the most solid export companies of the country with a production capacity of 70 thousand 70 kg bags per month."
                img={data.story_device_data.childImageSharp.fluid}
                videoSrcURL={null}
                videoTitle={null}
              />
              <Card
                i="5"
                title="Backed by Blockchain and IoT"
                p="With Internet of Things (IoT) sensors it is now possible to track produce packing and shipping conditions from start to finish. With this vital information secured by Penta blockchain technologies, LaMeseta Coffee is traceable around the world for highest quality products."
                img={data.story_sensor_tracking.childImageSharp.fluid}
                videoSrcURL={null}
                videoTitle={null}
              />
              <Card
                i="6"
                title="Coffee You Can Trust"
                img={data.story_trust.childImageSharp.fluid}
                videoSrcURL={null}
                videoTitle={null}
              >
                <BarChart />
              </Card>
              <Card
                i="7"
                title="LaMeseta Coffee Rewards"
                p="La Meseta is a premium coffee brand with a commitment to sourcing ethical coffee beans. We work with local farmers and cooperatives to ensure sustainable farming, fair labor practices, cultivating the very highest quality of Colombian coffee. With each sip you help us to support our farming communities and help us to sustain the land that brings you La Meseta Coffee."
                img={data.story_reward.childImageSharp.fluid}
                videoSrcURL={null}
                videoTitle={null}
                link={{
                  href: 'https://www.lameseta.com.co/buy-coffee/',
                  tag: 'Tap for a 10% Off Purchase Discount',
                }}
              />
            </Grid>
          </SubContainer>
        </Container>
      </Section>
    )}
  />
);

const SubContainer = styled(Container)`
  background-color: white;
  padding-top: 1rem;
  padding-bottom: 2rem;
`;

const Card = props => {
  const CardWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    @media (min-width: ${props => props.theme.screen.xs}) {
      flex: 0 1 100%;
    }
    @media (min-width: ${props => props.theme.screen.md}) {
      flex: 0 1 calc(50% - 1.5rem);
    }
    @media (min-width: ${props => props.theme.screen.lg}) {
      flex: 0 1 calc(33% - 1.5rem);
    }
    @media (min-width: ${props => props.theme.screen.xl}) {
      flex: 0 1 calc(25% - 1.5rem);
    }
  `;
  const CardInner = styled.div`
    width: 100%;
    max-width: 500px;
    @media (min-width: ${props => props.theme.screen.xs}) {
      padding: 0 1rem 2rem 1rem;
    }
    @media (min-width: ${props => props.theme.screen.md}) {
      padding: 0 0 2rem 0;
    }
  `;
  const H2Wrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: center;
    padding: 1rem 0.5rem 0 0;
    width: 100%;
    @media (max-width: ${props => props.theme.screen.md}) {
      align-items: baseline;
    }
  `;
  const H2 = styled.h2`
    padding: 0 0 0 1rem;
  `;
  const H2I = styled.span`
    background-color: ${props => props.theme.color.green.regular};
    color: white;
    ${props => props.theme.font_size.xlarge};
    border-radius: 4px;
    padding: 0 0.5rem;
  `;
  const P = styled.div`
    font-size: 1.05rem;
    line-height: 1.5;
    color: #564f62;
    padding: 1rem 0 0 0;
    text-align: justify;
    width: 100%;
    height: auto;
  `;
  const TagWrapper = styled.a`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
    padding-top: 1rem;
    color: #ff963b;
    text-underline-position: under;
    cursor: pointer;
  `;
  const ArtImg = styled(Img)`
    margin: 0;
    max-width: 500px;
    width: 100%;
    max-height: 323px;
  `;
  const Video = styled.div``;
  return (
    <CardWrapper>
      {props.img && <ArtImg fluid={props.img} />}
      {props.videoSrcURL && (
        <Video>
          <iframe
            width="100%"
            height="315px"
            src={props.videoSrcURL}
            title={props.videoTitle}
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            frameBorder="0"
            webkitallowfullscreen="true"
            mozallowfullscreen="true"
            allowFullScreen
          />
        </Video>
      )}
      <CardInner>
        <H2Wrapper>
          <H2I>{props.i}</H2I>
          <H2>{props.title}</H2>
        </H2Wrapper>
        {props.p && <P>{props.p}</P>}
        {props.children && <P>{props.children}</P>}
        {props.link && (
          <TagWrapper href={props.link.href} target="_blank">
            <StarBorder /> {props.link.tag}
          </TagWrapper>
        )}
      </CardInner>
    </CardWrapper>
  );
}; //eo card

const Grid = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-evenly;
`;

export default About;
