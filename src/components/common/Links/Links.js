import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';

//https://www.delish.com/cooking/g1968/easy-apple-recipes/?slide=14
//https://www.allrecipes.com/recipe/256016/dijon-pork-with-apples-and-cabbage/
import {
	LinksWrapper,
	Title,
	AnchorLinkWrapper,
	AnchorList,
	BackgroundWrapper1,
	BackgroundWrapper2,
	BackgroundWrapper3,
	BackgroundWrapper4
} from './style';
const Links = (props) => {
	const { links_AllRecipes_6531610, links_delish, links_pillsbury, links_tasteofhome } = useStaticQuery(
		graphql`
			query {
				links_AllRecipes_6531610: file(sourceInstanceName: { eq: "art" }, name: { eq: "AllRecipes_6531610" }) {
					childImageSharp {
						fluid(maxWidth: 100) {
							...GatsbyImageSharpFluid_withWebp
						}
					}
				}
				links_delish: file(
					sourceInstanceName: { eq: "art" }
					name: { eq: "delish_caramel-apple-ice-cream-cups-pinterest-still001-1535043180" }
				) {
					childImageSharp {
						fluid(maxWidth: 100) {
							...GatsbyImageSharpFluid_withWebp
						}
					}
				}
				links_pillsbury: file(
					sourceInstanceName: { eq: "art" }
					name: { eq: "Pillsbury_75593ed5-420b-4782-8eae-56bdfbc2586b" }
				) {
					childImageSharp {
						fluid(maxWidth: 100) {
							...GatsbyImageSharpFluid_withWebp
						}
					}
				}
				links_tasteofhome: file(
					sourceInstanceName: { eq: "art" }
					name: { eq: "TasteOfHome_Baked-Apple-Surprise_EXPS_LSBZ18_105111_C01_18_-3b-36" }
				) {
					childImageSharp {
						fluid(maxWidth: 100) {
							...GatsbyImageSharpFluid_withWebp
						}
					}
				}
			}
		`
	);
	// debugger;
	return (
		<LinksWrapper>
			<Title>RECIPE INSPIRATIONS</Title>
			<AnchorList>
				<AnchorLinkWrapper
					target="_blank"
					href={`https://www.allrecipes.com/recipe/256016/dijon-pork-with-apples-and-cabbage/`}
				>
					<BackgroundWrapper1 fluid={links_AllRecipes_6531610.childImageSharp.fluid} />
				</AnchorLinkWrapper>
				<AnchorLinkWrapper
					target="_blank"
					href={`https://www.delish.com/cooking/g1968/easy-apple-recipes/?slide=14`}
				>
					<BackgroundWrapper2 fluid={links_delish.childImageSharp.fluid} />
				</AnchorLinkWrapper>
				<AnchorLinkWrapper
					target="_blank"
					href={`https://www.pillsbury.com/recipes/perfect-apple-pie/1fc2b60f-0a4f-441e-ad93-8bbd00fe5334`}
				>
					<BackgroundWrapper3 fluid={links_pillsbury.childImageSharp.fluid} />
				</AnchorLinkWrapper>
				<AnchorLinkWrapper
					target="_blank"
					href={`https://www.tasteofhome.com/collection/healthy-apple-recipes/`}
				>
					<BackgroundWrapper4 fluid={links_tasteofhome.childImageSharp.fluid} />
				</AnchorLinkWrapper>
			</AnchorList>
		</LinksWrapper>
	);
};

export default Links;
